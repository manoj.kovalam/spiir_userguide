\chapter{Introduction}

SPIIR is a software routine (pipeline) used to search for gravitational wave signals from data of laser interferometers. It has been used in LIGO the first observing run (O1), the second observing run (O2), and the third observing run (O3) runs. From O3, it is selected as one of the five online pipelines to generate public alerts.

This document provides technical details of the software base, installation, and examples to use the SPIIR pipeline.

\section{Contributors}
Main contributors for the SPIIR package come from the GW data analysis group of UWA and collaborators from Tsinghua Univ., and Caltech.

\section{User requirements}
This document requires users to have basic knowledges of linux/mac terminals, computer clusters, computing languages including C, python, and Makefile. Understanding of GPU acceleration and CUDA language is preferred.

\section{Access of this document}

This document can be downloaded using the following command:

\begin{lstlisting}
git clone https://git.ligo.org/lscsoft/spiir.git spiir
git checkout spiir_userguide
\end{lstlisting}

Since the size of the compiled pdf document is too large to be uploaded frequently, only the source tex files for this document will be updated timely. To check out the latest source files, use the following command:
\begin{lstlisting}
git pull
\end{lstlisting}
You need to compile the entry source file main.tex using a Tex software, e.g. texshop, to generate the latest pdf version of the document.

The scripts for all the programs listed in this document are included in the source files. You can check out the source file "chapters/Chapterx.tex" to find where the scripts are.

\section{Package overview}
The SPIIR package bears the name \verb;gstlal-spiir; in the code repository (\href{https://git.ligo.org/lscsoft/spiir}{link}). It is based on \verb;gstlal; library (\href{https://git.ligo.org/lscsoft/gstlal}{link}). \verb;gstlal; is an open-source low-latency tool library developed for the gravitational wave (GW) data analysis group. The name \verb;gst; of \verb;gstlal; represents the multimedia software library --- \verb;gstreamer; (\href{https://gstreamer.freedesktop.org/}{link}) and the name \verb;lal; (\href{https://git.ligo.org/lscsoft/lalsuite}{link}) is from the GW analysis software library --- the LIGO Algorithm Library (LAL) suite. 

\subsection{Introduction to a pipeline}
The techincal term "pipeline" here refers to a processing routine that directly takes data in and output useful information after several processing modules. The \verb;gstreamer; library is used here to realize foundations for the SPIIR pipeline. The basic feature of \verb;gstreamer; is a \textit{buffer}, which is a small segment of data with meta information about the data. Meta-information usually includes the duration, the timestamp, and the format of the data. \textit{buffer}s are taken in and processed in functional units called \textit{elements}. %We refer readers to~\cite{gstreamer_doc} for a complete documentation of this library. 

A \textit{pipeline} is made by linking a series of \textit{elements}. There are two basic requirements for a pipeline to work. The first requirement is that the data formats that adjacent elements can process need to be compatible. The other is that there needs to be an element to trigger the pipeline processing --- this element is usually the first element that handles the data interface. 

When the pipeline starts, \textit{buffer}s will one by one flow through \textit{elements}. The data flow is managed by a higher level \textit{event} schedule mechanism. This \textit{event}, different from a GW event, is a \verb;gstreamer; function that notifies the specific element to start runnning.

To achieve real-time processing, the processing time of a data \textit{buffer} in an \textit{element} needs to be less than the duration of the \textit{buffer} this element reads in, in order to make sure the latency will not be accumulating. %The latency of the SPIIR pipeline is not fixed, as the computation of the coincidence or coherent processing component varies with the noise behavior of the data. The estimated latency distribution over the main pipeline components is presented in Tab.~\ref{tab:pipe_latency}.

\iffalse
\subsection{Package directory tree}
Here we show the directory of the software package that contains the construction of the offline and online pipeline and the code for SPIIR filtering and coherent analysis. The source files can be browsed at: \href{https://git.ligo.org/lscsoft/spiir}.
(The source files are under review and subject to change.)

The directory is \verb;gstlal-spiir;:
\dirtree{%
 .1 gstlal-spiir.
 .2 bin.
 .3 gstlal\_inspiral\_postcohspiir\_online $\heartsuit$.
 .3 gstlal\_inspiral\_postcohspiir\_offline $\heartsuit$.
 .3 gstlal\_inspiral\_postcohspiir\_lvalert\_background\_plotter.
 .3 gstlal\_cohfar\_plot\_ifar $\clubsuit$.
 .3 gstlal\_cohfar\_skymap2fits.
 .2 gst.
 .3 cohfar.
 .4 background\_stats.h $\clubsuit$.
 .4 background\_stats\_utils.c $\clubsuit$.
 .4 background\_stats\_utils.h $\clubsuit$.
 .4 cohfar\_accumbackground.c $\clubsuit$.
 .4 cohfar\_accumbackground.h $\clubsuit$.
 .4 cohfar\_assignfar.c $\clubsuit$.
 .4 cohfar\_assignfar.h $\clubsuit$.
 .4 ssvkernel.c $\clubsuit$.
 .4 ssvkernel.h $\clubsuit$.
 .4 test.
 .3 cuda.
 .4 spiir.
 .5 spiir.c $\clubsuit$.
 .5 spiir.h $\clubsuit$.
 .5 spiir\_kernel.cu $\clubsuit$.
 .5 spiir\_kernel.h $\clubsuit$.
 .4 multiratespiir.
 .5 multiratespiir.c $\clubsuit$.
 .5 multiratespiir.h $\clubsuit$.
 .5 multiratespiir\_kernel.cu $\clubsuit$.
 .5 multiratespiir\_kernel.h $\clubsuit$.
 .5 multiratespiir\_utils.c $\clubsuit$.
 .5 multiratespiir\_utils.h $\clubsuit$.
 .5 test.
 .4 postcoh.
 .5 postcoh\_utils.c $\clubsuit$.
 .5 postcoh\_utils.h $\clubsuit$.
 .5 postcohinspiral\_table\_utils.c $\clubsuit$.
 .5 postcohinspiral\_table\_utils.h $\clubsuit$.
 .5 postcohinspiral\_table.h $\clubsuit$.
 .5 postcoh.c $\clubsuit$.
 .5 postcoh.h $\clubsuit$.
 .5 postcoh\_kernel.cu $\clubsuit$.
 .5 postcoh\_kernel.h $\clubsuit$.
 .5 postcoh\_filesink.c $\clubsuit$.
 .5 postcoh\_filesink.h $\clubsuit$.
 .5 test.
 .3 spiir.
 .4 spiir.c $\clubsuit$.
 .4 spiir.h $\clubsuit$.
 .2 python.
 .3 postcoh\_finalsink.py $\clubsuit$.
 .3 postcoh\_table\_def.py $\clubsuit$.
 .3 spiirparts.py $\heartsuit$.
}

\fi
