#!/bin/bash
set -e
datatype=cleaned
starttime=1186600000
endtime=1187318000

if [ "$datatype" == "cleaned" ]; then
  ChannelNameHL=DCH-CLEAN_STRAIN_C02
  ChannelNameV=Hrec_hoft_V1O2Repro2A_16384Hz
  if [ ! -f "frame.cache.${datatype}" ]; then ####TODO: CHECK IF datatype CHANGED
    gw_data_find -t L1_CLEANED_HOFT_C02 -o L -s ${starttime} -e ${endtime} -u file -l > "frame.cache.${datatype}"
    gw_data_find -t H1_CLEANED_HOFT_C02 -o H -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
    gw_data_find -t V1O2Repro2A -o V -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
  fi
elif [ "$datatype" == "0817cleaned" ]; then
  ChannelNameHL=DCH-CLEAN_STRAIN_C02
  ChannelNameV=Hrec_hoft_V1O2Repro2A_16384Hz
  if [ ! -f "frame.cache.${datatype}" ]; then ####TODO: CHECK IF datatype CHANGED
    gw_data_find -t L1_CLEANED_HOFT_C02_T1700406_v3 -o L -s ${starttime} -e ${endtime} -u file -l > "frame.cache.${datatype}"
    gw_data_find -t H1_CLEANED_HOFT_C02 -o H -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
    gw_data_find -t V1O2Repro2A -o V -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
  fi
elif [ "$datatype" == "0608clean" ]; then
  ChannelNameHL=DCS-CALIB_STRAIN_C01_T1700372_v2
  if [ ! -f "frame.cache.${datatype}" ]; then ####TODO: CHECK IF datatype CHANGED
    gw_data_find -t L1_HOFT_C01_T1700372_v2 -o L -s ${starttime} -e ${endtime} -u file -l > "frame.cache.${datatype}"
    gw_data_find -t H1_HOFT_C01_T1700372_v2 -o H -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
  fi
elif [ "$datatype" == "C02" ]; then
  ChannelNameHL=DCS-CALIB_STRAIN_C02
  ChannelNameV=Hrec_hoft_16384Hz
  if [ ! -f "frame.cache.${datatype}" ]; then ####TODO: CHECK IF datatype CHANGED
    gw_data_find -t L1_HOFT_C02 -o L -s ${starttime} -e ${endtime} -u file -l > "frame.cache.${datatype}"
    gw_data_find -t H1_HOFT_C02 -o H -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
    gw_data_find -t V1O2Repro1A -o V -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}" ####TODO: CHECK V1 C02 type
  fi
elif [ "$datatype" == "C00" ]; then
  ChannelNameHL=GDS-CALIB_STRAIN
  ChannelNameV=Hrec_hoft_16384Hz
  if [ ! -f "frame.cache.${datatype}" ]; then ####TODO: CHECK IF datatype CHANGED
    gw_data_find -t L1_HOFT_C00 -o L -s ${starttime} -e ${endtime} -u file -l > "frame.cache.${datatype}"
    gw_data_find -t H1_HOFT_C00 -o H -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
    gw_data_find -t V1Online -o V -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
  fi
elif [ "$datatype" == "O2Replay" ]; then
  ChannelNameHL=GDS-CALIB_STRAIN_O2Replay
  ChannelNameV=Hrec_hoft_16384Hz_O2Replay
  if [ ! -f "frame.cache.${datatype}" ]; then ####TODO: CHECK IF datatype CHANGED
    gw_data_find -t L1_O2_llhoft -o L -s ${starttime} -e ${endtime} -u file -l > "frame.cache.${datatype}"
    gw_data_find -t H1_O2_llhoft -o H -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
    gw_data_find -t V1_O2_llhoft -o V -s ${starttime} -e ${endtime} -u file -l >> "frame.cache.${datatype}"
  fi
else
  echo "Incorrect datatype specified"
  exit 1
fi
