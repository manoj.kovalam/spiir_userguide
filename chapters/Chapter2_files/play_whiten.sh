eventtime=1186624818
gpsstart=$((eventtime - 200))
gpsend=$((eventtime + 300))
framefile=zero_inj.cache
channelname=FAKE-STRAIN
ifo=H1
psd=../H1L1V1-REFERENCE_PSD-1186624818-511200.xml.gz
gstlal_play \
	--output data_${ifo}.txt \
	--whiten \
	--reference-psd $psd \
	--gps-start-time $gpsstart \
	--gps-end-time $gpsend \
	--frame-cache $framefile \
	--data-source frames \
	--channel-name ${ifo}=$channelname \
	--rate 2048 \
	--verbose

