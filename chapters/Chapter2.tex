\chapter{Data generation}

This chapter will show how to generate an injection set which include a list of injection parameters that can be injected into data (Sec.~\ref{sec:an_injection_set}). It gives the way to generate silent data, with injected waveforms from an injection set (Sec.~\ref{sec:emp_data_inj}). It gives the way to generate Gaussian noise data from advanced LIGO/Virgo power spectral density (PSD, with or without injections (Sec.~\ref{sec:gauss_data_inj}). It gives the way to find real data from LIGO cluster (Sec.~\ref{sec:real_data}). It gives the way to measure the PSD from existing data, whether it Gaussian or real data (Sec.~\ref{sec:psd}). It also provides the program to whiten the data and output it in text format (Sec.~\ref{sec:data_whiten}).

\section{An injection set}
\label{sec:an_injection_set}
The following is a "Makefile.inj" file that can generate an injection file "bns\_inj\_1.xml" by "make -f Makefile.inj". The output xml file will give a list of injections starting Aug 13st, 2:0:0 2017 UTC and injections 3000 seconds apart. The command "lalapps\_inspinj" can also use a power spectral density (PSD) to generate injections at given signal-to-noise ratios (SNRs). To check out all available options for the injection parameters, execute "lalapps\_inspinj --help".
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/Makefile.inj}

Use "ligolw\_print" command to print parameters for the injections. E.g.:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/print_paras.sh}
This will print the end times of individual detectors, masses, and effective distances of the source.

The output parameters for the first injection of "bns\_inj\_1.xml" after execution of the above command is:
\begin{table}[htb]
\caption[]{"h\_end\_time" and "h\_end\_time\_ns" are the end time of the injection in GPS seconds and GPS nano seconds in LIGO-H detector. "alpha4" is the computed expected SNR in LIGO-H detector, as explained in the last section of this chapter.}
 \resizebox{\textwidth}{!}{% use resizebox with textwidth
  \begin{tabular}{@{} |ccccccccccccc| @{}}

    \hline
     & h\_end\_time & h\_end\_time\_ns & mass1 & mass2 & mchirp & eta & spin1z & spin2z & eff\_dist\_h & alpha4 & longitude  & latitude \\ 
    \hline
    bns\_inj\_1.xml & 1186624818 & 12168969 & 2.93 & 1.81 & 1.99 & 0.24 & -0.10 & -0.32 & 32.5 & 41.0 & 1.99 & -0.38 \\ 
    \hline
  \end{tabular}
}
\label{tab:inj1_prop}
 \end{table}

\subsubsection{Calculate expected SNR for injections}

Use "gstlal\_inspiral\_injection\_snr" to calculate expected SNR with a power spectral density (PSD). The expected SNRs for LIGO-H, LIGO-L, and Virgo will be filled in the fields of "alpha4", "alpha5", and "alpha6" of the injection file, e.g. "bns\_inj\_1.xml" in the following example. 
\begin{lstlisting}
gstlal_inspiral_injection_snr --injection-file bns_inj_1.xml --flow 20.0 --reference-psd-cache psd.cache
\end{lstlisting}

The "psd.cache" lists the locations of the PSD files. In this example, the format of the file is shown below:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/psd.cache}


\section{Empty data with injections}
\label{sec:emp_data_inj}
Use "gstlal\_fake\_frames" to generate fake data with options like silence, Gaussian data, with or without injections. The output will be a directory given by "--output-path" with frame files in it. Need to specify channel name, frame type and GPS times.

\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/gen_zerodata_withinj.sh}

\section{Simulated Gaussian advanced LIGO noise with or without injections}
\label{sec:gauss_data_inj}
Use "gstlal\_fake\_frames" to generate fake advanced LIGO noise with or without injections. The output will be a directory given by "--output-path" with frame files in it.


\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/gen_simdata_withinj.sh}
\section{Real data}
\label{sec:real_data}
Use "gw\_data\_find" on a LIGO Data Grid cluster to find real offline data. The output will be a file in certain format that lists the duration and locations of the frames. The online/real-time data is transferred and distributed to shared-memory of computing nodes through llldd and kafka mechanisms. C00 offline data is equivalent to the online data that have been used in the online runs. Here is the script that can find various versions of data:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/datatypes.sh}



\section{Estimation and plotting the PSD of the data}
\label{sec:psd}
gstlal\_reference\_psd is the program to estimate the median PSD of data given a period of time. It uses the lal\_whiten plugin to estimate the PSD. E.g.\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/make_psd.sh}

Use "gstlal\_plot\_psd" to plot the estimated PSD:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/plot_psd.sh}

An example of PSD from this command is shown below.

\begin{figure}[!h]
\centering
\includegraphics[width=.9\textwidth]{chapters/Chapter2_files/review_psd.png}
\caption[]{PSD plot from estimated PSD using gstlal\_reference\_psd program. The legend shows the horizon distance for each detector, LIGO-H (H), LIGO-L (L), and Virgo (V).}
\label{fig2:psd}
\end{figure}

\section{Data whitening}
By default, the conventional FFT method is used for whitening (lal\_whiten plugin). This introduce a delay for collection of data. The time-domain FIR whitening (lal\_tdwhiten plugin) currently has two issues, one is generating many spikes, the other is the phase issue.

"gstlal\_play" is a simple program to output data from frame files to text format with options to perform simple functions including high pass filtering, low pass filtering, and whitening. The detection pipeline also performs whitening at the beginning of its processes. You can also obtain the whitened data product in the same text format from the detection pipeline. See Sec.~\ref{sec:pipe_dump}.
\subsubsection{Whitening using gstlal\_play}
\label{sec:data_whiten}
Use "gstlal\_play" command to whiten the data. 
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/play_whiten.sh}

The output of the above script is a text file with two columns. One column for the timestamp at each sample time, the other is the corresponding whitened H1 data. A simple python script can plot this txt file. The figure below shows the whitened data for H1, L1, and Virgo detectors.
\begin{figure}[!h]
\centering
\includegraphics[width=.9\textwidth]{chapters/Chapter2_files/wdata_0.png}
\caption[]{Data applied whitening with an O2 PSD, LIGO-H (top), LIGO-L (middle), and Virgo (bottom). Each data contains an injection without any noise. The injected signal suffered the most after Virgo PSD whitening due to that the Virgo noise is the worst of the three.}
\label{fig2:wdata}
\end{figure}

\subsubsection{Whitening as part of the detection pipeline}
\label{sec:pipe_dump}
The current detection pipeline "gstlal\_inspiral\_postcohspiir\_online" provides the option to dump intermediate data products. To do that, just simply add "--nxydump-segment GPS\_START\_TIME:GPS\_END\_TIME" to the options. Each data product consists of the timestamp column and column or columns for data values. It will output:
\begin{enumerate}
    \item before\_highpass\_data\_IFO1\_GPSSTARTTIME.dump. This shows the raw data that has been applied segment vetoes.
    \item If the pipeline has been input a FIR whitening option (--fir-whitener 1),  two products will be output: after\_highpass\_data\_IFO1\_GPSSTARTTIME.dump showing data after the highpass and after\_tdwhiten\_data\_IFO1\_GPSSTARTTIME.dump showing data after the tdwhitener function following highpass. If the pipeline uses the FFT whitening (default or --fir-whitener 0), there is output after\_fdwhiten\_data\_IFO1\_GPSSTARTTIME.dump. This shows the data that has been whitened using FFT with the block size given by the --psd-fft-length option.
    \item after\_htgate\_data\_IFO1\_GPSSTARTTIME.dump. This shows the data that have been whitened, either td or fd, been applied the segment vetoes again and been zeroed-out for any spikes above the --ht-gate-threshold.
    \item snr\_gpu\_GSPSTARTTIME\_IFO1\_GPUSTREAMID.dump. Following the last process, this shows data been filtered with a SPIIR bank where GPUSTREAMID is the GPU stream ID allocated for this bank job. 
\end{enumerate}
Words in upper cases are explained here: IFO is one of the H, L or V. GPSSTARTTIME is the GPS\_START\_TIME set at the option, The data product after\_htgate\_data\_IFO1\_GPSSTARTTIME.dump will be used in Sec.~\ref{sec:mf_fft} for a standalone matched filtering program.
    






