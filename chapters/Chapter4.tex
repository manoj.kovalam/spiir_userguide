\chapter{Matched/SPIIR filtering}

We denote signal-to-noise ratio (SNR) by $\varrho$, the whitened data by $d$ and the whitened template by $h$. The definition of SNR is:
\begin{equation}
\varrho (t) = \int d(t)  h(t-\tau) d \tau \equiv d(t) \otimes h(-t),
\end{equation}
where $\otimes$ is the convolution symbol. 

There is an efficient way to calculate SNR. That is to explore the convolution theorem and utilize the Fast Fourier Transform (FFT) method.
\begin{equation}
    d(t) \otimes h(-t) = \mc{F}^{-1} \left ( \mc{F}(d) \mc{F}(h) \right),
\end{equation}
where $\mc{F}$ denotes the FFT.

For SPIIR filtering, check out references TODO.


\section{Matched filtering using FFT}
\label{sec:mf_fft}
Whitened data can be obtained from the pipeline using the method in Sec.~\ref{sec:pipe_dump}. The "gstlal\_matched\_filter" program is a standalone program to filter the whitened data with a whitened template and output two key statistics --- SNR, and $\chi^2$. This will help validate that these two statistics calculated from the pipeline be accurate.  It can take two formats for the templates. One is whitened template in text format. The other is the SPIIR bank in xml format. See below the two examples.

The outputs are verbose printouts showing maximum SNR, its real and imaginary values, and the index of the maximum SNR of the SNR series. It also provides options to generate plots for the input template, the output SNR series. It can only provide the $\chi^2$ plot in the following second method at the moment. The SNR is generated using the FFT.

\subsubsection{Template waveform as input}
If option of "--template-format" is "waveform\_template", the program will read the whitened template in text format and compute SNR.  Whitened templates in text format can be generated as instructed in Sec.~\ref{sec:spiir_filt_gen}. The verbose output is rendered in the Table.~\ref{tab:mf_snrs}.  With --data-prefix option, it will output a figure shown in Fig.~\ref{fig:data_tmplt}. With --snr-prefix option, it will generate the SNR plot shown in Fig.~\ref{fig:snr_mf}.

\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter4_files/matched_filt.sh}

\begin{table}[htb]
\label{tab:mf_snrs}
\caption[]{Filtering results (i.e. SNR) with different templates for injection one data using "gstlal\_matched\_filter". Note the SPIIR reconstructed response is from an unoptimized set of SPIIR filters which has an overlap with the SPIIR template of 98\% (see Sec.~\ref{sec:spiir_filt_gen}.). The expected SNR is 41.0 as shown in Sec.~\ref{tab:inj1_prop}.}
 \resizebox{\textwidth}{!}{% use resizebox with textwidth
  \begin{tabular}{@{} |c|c|c|c|c| @{}}

    \hline
     & SNR & $\sigma$  & estimated eff dist & h\_end\_time\\ 
    \hline
    FIR template & 40.86 & 952.6 & 23.3 & 1186624818.011718750\\ 
    \hline
    SPIIR template & 40.82 & 951.6 & 23.3 & 1186624818.011718750 \\
    \hline
    SPIIR reconstructed response & 39.99 & 934.2 & 23.4 & 1186624818.011718750 \\
    \hline
  \end{tabular}
}
 \end{table}

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{chapters/Chapter4_files/data_spiir_tmplt_fft.png}
\caption[]{The whitened data (above) and the whitened SPIIR template (bottom) in the Fourier domain from the gstlal\_matched\_filter program with the --data-prefix option.}
\label{fig:data_tmplt}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{chapters/Chapter4_files/snr_spiir_tmplt_H1.png}
\caption[]{The SNR series in time domain from gstlal\_matched\_filter program with the --snr-prefix option.}
\label{fig:snr_mf}
\end{figure}

\subsubsection{SPIIR bank as input}
If input of "template-format" is "spiir\_bank", it will read the SPIIR filters from the bank for SNR computing and read the auto-correlation matrix from that for $\chi^2$ calculation. It can generate outputs as mentioned in the last section. When --chisq-prefix input is given, it can generate the verbose printout for the $\chi^2$ degree of freedom and $\chi^2$ value that should be consistent with the pipeline. In that setting, it will also plot elements for the $\chi^2$ as shown in Fig.~\ref{fig:chisq_mf}.


\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter4_files/test_auto.sh}

\begin{figure}[!h]
\centering
\includegraphics[width=.8\textwidth]{chapters/Chapter4_files/chisq_iir.png}
\caption[]{Elements for $\chi^2$ calculation from the gstlal\_matched\_filter program with --chisq-prefix. Above is the auto-correlation array read from the bank; middle is the real part of the SNR series (blue) against that of the expected SNR series (orange) projected from the peak SNR at 0.0 time; bottom is the imaginary part of the SNR series against that of the expected SNR series. Here the auto-correlation length is 951 samples. The $\chi^2$ value in this case is 0.56.}
\label{fig:chisq_mf}
\end{figure}


\section{A simple SPIIR filtering pipeline}


\subsection{Code base}
"gstlal\_inspiral\_spiir" is a light-weight pipeline that only filters the data with SPIIR filters and not form any candidates. Due to the large size of the automatically generated flowchart when the gstreamer options turned on, the flowchart of this pipeline is not shown here but given atx \href{https://git.ligo.org/lscsoft/spiir/tree/spiir_userguide/v0.1/chapters/Chapter4_files/mrspiir_trig.png}{this link}.
\begin{table*}[ht]
\caption{This SPIIR pipeline and corresponding \textsf{gstreamer}, \textsf{gstlal} or SPIIR software plugins. Each plugin is implemented in several code files in C langauage or Python language listed in the last column. The pipeline executable code is \textsf{gstlal\_inspiral\_spiir}.}
\scriptsize
%\resizebox{0.8\textwidth}{!}{
\begin{tabular}{|p{3cm}|p{1.2cm}|p{3cm}|p{3cm}|p{3cm}|}
\hline
SPIIR pipeline modules & gstreamer plugin & gstlal plugin & SPIIR plugin & code\\
\hline
Read from shared memory & & gds\_lvshmsrc, framecpp\_channeldemux & & \\
\hline
Apply state\_vector, DQ channel, &  & lal\_statevector, lal\_gate & & \\
\hline
Data whitening & & lal\_whiten, lal\_tdwhiten & & \\
\hline
Gating & & lal\_gate & & \\
\hline
Multirate SPIIR bank filtering & & & cuda\_multiratespiir & spiirparts.py, multiratespiir.c, multiratespiir.h, multiratespiir\_kernel.cu, multiratespiir\_utils.h, multiratespiir\_utils.c\\
\hline
App sink & appsink & & &\\
\hline
\end{tabular}
%}
\label{tab:spiirfilt_codebase}
\end{table*}

\subsection{Example of the GW170817 event}
\label{sec:spiir_pipe}
An example of using this pipeline is set up at OzStar \\ (/fred/oz016/chichi/unit\_test/test\_gw170817\_spiir\_filt/run\_bank3\_multratespiir.sh). You can copy the example over and execute it in your own directory. The outputs are "dot" files that can be converted to "png" format to display the pipeline flowcharts (as shown in the beginning of this section) and the "*.dump" files that give data dumps at various stages of the pipeline.

\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter4_files/run_bank3_multiratespiir.sh}

The flowchart of the pipeline can be generated from the output of the pipeline by the script below:

\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter4_files/gen_png.sh}

Use the following python script, we can plot the SNR dumps from the output of the pipeline:

\lstinputlisting[language=Python]{chapters/Chapter4_files/plot_snrseries.py}

\begin{figure}[!h]
\centering
\includegraphics[width=.8\textwidth]{chapters/Chapter4_files/snr_1187008880_3.png}
\caption[]{Plot of the output SNR dumps from the gstlal\_inspiral\_spiir program. The maximum SNRs for H1, L1, and V1 are 15.84, 23.29, and 4.34 respectively for this event.}
\label{fig:snr_dump}
\end{figure}

\section{Examples}
\subsection{Silent noise with injection}
The data is prepared with injection 
\subsection{SNR using early warning templates}
The original template is cut at 15 seconds before the merger time for SNR and $\chi^2$ computing. 

\section{Debug the pipeline}

The first debug tool is to use Gstreamer's default check point method. You insert GST\_xx clauses to the code where you think need to be check pointed. E.g. if we insert GST\_LOG("print this line") in the cuda\_multiratespiir plugin c file. After compilation, you just need to add two flags into the pipeline run script: ``--gst-debug plugin\_name:5 --gst-debug-no-color". When executed, the pipeline will print out this line message. With this tool, we can print out any check points setup at any plugin. The instruction can be found in \href{https://gstreamer.freedesktop.org/documentation/tutorials/basic/debugging-tools.html?gi-language=c}{this link}. The cons of the method is when problems happen in other places, we are not able to show information there.

We can use ``printf" to print out information in other places. Or when the pipeline program exits abnormally with a segmentation fault (seg-fault), we use the tool ``gdb" to find the place that caused the seg-fault. 

There are two ways to use ``gdb" to access the break/program exit point for a seg-fault program. The first way is to use the core dump. Set the core dump limit to be unlimited. There would be a core dump file after the execution, e.g. core.1916. Then use ``gdb python core.1916" to debug the core dump. Use ``bt" (backtrace) command to find the code stack/location for the exit point, it will list the call sequence (call graph) for the fault. Use ``frame xx" you can access values of variables at different call stack. The other way is to reproduce the seg-fault inside gdb and access the exit point. The procedure is first to type ``gdb python", then type "run the\_full\_program\_command". You will end up with the same exit point as the same way.

\lstinputlisting[language=Bash]{chapters/Chapter4_files/debug_segfault_part1}

Use ``frame 1" you can go to the the first function call and print out variables:

\lstinputlisting[language=Bash]{chapters/Chapter4_files/debug_segfault_part1}

An example of a successful run is shown for comparison. 

\lstinputlisting[language=Bash]{chapters/Chapter4_files/debug}


