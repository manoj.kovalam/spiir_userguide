
pipesky=H1L1V1_skymap/L1_1187008882_445800781_3_806
outsnr=gw170817_3det_snr.fits
outprob=gw170817_3det_prob.fits
etime=1187008882
out_snrpng=gw170817_3det_snrmap.png
out_probpng=gw170817_3det_probmap.png
detrsp_map=/fred/oz016/chichi/unit_test/test_gw170817/H1L1V1_detrsp_map.xml


gstlal_postcoh_skymap2fits --output-cohsnr $outsnr --output-prob $outprob --event-time $etime --cuda-postcoh-detrsp-fname $detrsp_map --event-id GW170817 $pipesky
bayestar_plot_allsky_postcohspiir -o $out_snrpng $outsnr --colormap nipy_spectral --colorbar --radec 197.45 -23.33 --contour 90 --label "Coherent SNR"
bayestar_plot_allsky_postcohspiir -o $out_probpng $outprob --radec 197.45 -23.33 --contour 50 90 --annotate 
