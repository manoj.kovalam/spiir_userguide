import matplotlib
matplotlib.use('Agg')
from matplotlib import rc
import numpy as np
import matplotlib.pyplot as plt
import os, re
import pdb

rc('text', usetex = True)
##########################
# initialization #
##########################
ndet = 2
plot_ifos = ['H1', 'L1']
plot_ifo_pairs = ['H1L1']
#far_thresh = 1./30/86400
far_thresh = 1
mass_crite_on_gstlaldet = False
mass_crite_on_injection = True
#sim_name = "/home/spiir/review/bns_set2_gstlalinj_clean/2det_gstlalpsdwhiten/run_summary/log_snr_gstlalpsd"
sim_name = "/home/spiir/review/injection_files/log_snr_bns"
bank_mass = "/home/spiir/review/bns_set2_gstlalinj_clean/2det_gstlalpsdwhiten/run_summary/log_mass"
spiir_result_dir = "."
#fsngl_gstlal_name = '/home/spiir/review/bns_set2_gstlalinj_clean/2det_gstlalpsdwhiten/run_summary/sim_sngl_far_gstlal'
fsngl_gstlal_name = "/home/spiir/review/compare/cody_sqlite/sim_sngl_far_gstlal_withtime"
missed_fn = "log_spiir_lowchisq"
plot_mchirp_min = 0.6
plot_mchirp_max = 2.3

##########################
# end #
##########################

def plot_missfound(inj, gstlal_det, spiir_det, far_name):
	found_mchirp = {} 
	found_dist = {} 
	miss_mchirp = {}
	miss_dist = {}
	found_mchirp.setdefault('spiir',[])
	found_dist.setdefault('spiir',[])
	miss_mchirp.setdefault('spiir',[])
	miss_dist.setdefault('spiir',[])
	found_mchirp.setdefault('gstlal',[])
	found_dist.setdefault('gstlal',[])
	miss_mchirp.setdefault('gstlal',[])
	miss_dist.setdefault('gstlal',[])
	for time in inj.keys():
		if time in spiir_det.keys() and spiir_crite(spiir_det[time], far_thresh):
			found_mchirp['spiir'].append(inj[time]['mchirp'])
			found_dist['spiir'].append(inj[time]['eff_dist_l'])
		else:
			miss_mchirp['spiir'].append(inj[time]['mchirp'])
			miss_dist['spiir'].append(inj[time]['eff_dist_l'])

		if time in gstlal_det.keys() and gstlal_det[time].values()[0]['far']< far_thresh:
			found_mchirp['gstlal'].append(inj[time]['mchirp'])
			found_dist['gstlal'].append(inj[time]['eff_dist_l'])
		else:
			miss_mchirp['gstlal'].append(inj[time]['mchirp'])
			miss_dist['gstlal'].append(inj[time]['eff_dist_l'])

	plt.figure()
	fig,axs = plt.subplots(2, 1)
	fig.subplots_adjust(hspace = .5)
	output = "missfound_spiir_gstlal_%s.png" % far_name
		
	for (iifo, item) in enumerate(['spiir', 'gstlal']):
		axs[iifo].scatter(miss_mchirp[item], miss_dist[item], c = 'k', label = '%s Missed' % item)
		axs[iifo].scatter(found_mchirp[item], found_dist[item], c = 'b', label = '%s Found' % item)
		axs[iifo].legend(loc='upper left')
		axs[iifo].set_xlabel("inj mchirp")
		axs[iifo].set_ylabel("inj effective distance in L")
		axs[iifo].set_ylim([-5,900])
		axs[iifo].set_xlim([plot_mchirp_min,plot_mchirp_max])
		axs[iifo].set_title("%s Missed/Found" % (item))
		fig.suptitle("spiir and gstlal Missed/Found" )
		plt.savefig(output)


def plot_inbank_det(item_name, cat_name, far_name):

	ntrig = len(eval(cat_name).keys())
	if ntrig == 0:
		return
	pipeline_name = "spiir"
	if item_name == 'snr':
		# inj vs. spiir
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_%s_%s_%s_hist.png" % (item_name, pipeline_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			pipeline_item = [eval(cat_name).values()[idx]['spiir'][spiir_item_name] for idx in range(0, ntrig)]
			inj_item = [eval(cat_name).values()[idx]['inj'][spiir_item_name] for idx in range(0, ntrig)]
			ratio = [pipeline_item[idx]/inj_item[idx] for idx in range(0, ntrig)]
			axs[iifo].hist(ratio, bins = 40, weights = np.ones(len(ratio))/len(ratio))
			axs[iifo].set_xlabel("spiir/inj %s" % item_name)
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(ratio)))
		fig.suptitle("%s/inj %s ratio in %s" % (pipeline_name, item_name, cat_name.replace('_', '')))
		plt.savefig(output)
		
		# inj vs. gstlal
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_gstlal_%s_%s_hist.png" % (item_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			gstlal_item = [eval(cat_name).values()[idx]['gstlal'][ifo]['snr'] if ifo in eval(cat_name).values()[idx]['gstlal'].keys() else 0 for idx in range(0, ntrig) ]
			inj_item = [eval(cat_name).values()[idx]['inj'][spiir_item_name] for idx in range(0, ntrig)]
			ratio = [gstlal_item[idx]/inj_item[idx] if gstlal_item[idx] > 0 else 0 for idx in range(0, ntrig)]
			axs[iifo].hist(ratio, bins = 40, weights = np.ones(len(ratio))/len(ratio))
			axs[iifo].set_xlabel("gstlal/inj %s" % item_name)
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(ratio)))
		fig.suptitle("gstlal/inj %s ratio in %s" % (item_name, cat_name.replace('_', '')))
		plt.savefig(output)

		# inj vs. spiir with inj SNR > 8
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_%s_%s_%s_hist_over8.png" % (item_name, pipeline_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			time_range = []
			for time_idx in eval(cat_name).keys():
				if eval(cat_name)[time_idx]['inj'][spiir_item_name]>8:
					time_range.append(time_idx)
			pipeline_item = [eval(cat_name)[time_idx]['spiir'][spiir_item_name] for time_idx in time_range]
			inj_item = [eval(cat_name)[time_idx]['inj'][spiir_item_name] for time_idx in time_range]
			ratio = [pipeline_item[idx]/inj_item[idx] for idx in range(0, len(pipeline_item))]
			axs[iifo].hist(ratio, bins = 20, weights = np.ones(len(ratio))/len(ratio))
			axs[iifo].set_xlabel("spiir/inj %s" % item_name)
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(ratio)))
		fig.suptitle("%s/inj %s ratio in %s (inj snr over 8)" % (pipeline_name, item_name, cat_name.replace('_', '')))
		plt.savefig(output)

	
		# inj vs. gstlal with inj SNR > 8
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_gstlal_%s_%s_hist_over8.png" % (item_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			time_range = []
			for time_idx in eval(cat_name).keys():
				if eval(cat_name)[time_idx]['inj'][spiir_item_name]>8:
					time_range.append(time_idx)
			gstlal_item = [eval(cat_name)[time_idx]['gstlal'][ifo]['snr'] if ifo in eval(cat_name)[time_idx]['gstlal'] else 0 for time_idx in time_range]
			inj_item = [eval(cat_name)[time_idx]['inj'][spiir_item_name] for time_idx in time_range]
			ratio = [gstlal_item[idx]/inj_item[idx] for idx in range(0, len(gstlal_item))]
			axs[iifo].hist(ratio, bins = 20, weights = np.ones(len(ratio))/len(ratio))
			axs[iifo].set_xlabel("gstlal/inj %s" % item_name)
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(ratio)))
		fig.suptitle("%s/inj %s ratio in %s (inj snr over 8)" % ("gstlal", item_name, cat_name.replace('_', '')))
		plt.savefig(output)

		# gstlal vs. spiir
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_gstlal_%s_%s_%s_hist_over8.png" % (item_name, pipeline_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			time_range = []
			for time_idx in eval(cat_name).keys():
				if eval(cat_name)[time_idx]['inj'][spiir_item_name]>8:
					time_range.append(time_idx)
			pipeline_item = [eval(cat_name)[time_idx]['spiir'][spiir_item_name] for time_idx in time_range]
			gstlal_item = [eval(cat_name)[time_idx]['gstlal'][ifo]['snr'] if ifo in eval(cat_name)[time_idx]['gstlal'] else 0 for time_idx in time_range]
			ratio = [pipeline_item[idx]/gstlal_item[idx] if gstlal_item[idx] > 0 else 0 for idx in range(0, len(pipeline_item))]
			axs[iifo].hist(ratio, bins = 20, weights = np.ones(len(ratio))/len(ratio))
			axs[iifo].set_xlabel("spiir/gstlal %s" % item_name)
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(ratio)))
		fig.suptitle("%s/gstlal %s ratio in %s (inj snr over 8)" % (pipeline_name, item_name, cat_name.replace('_', '')))
		plt.savefig(output)





def plot_sngl_det(item_name, cat_name, far_name):

	if 'spiir' in cat_name:
		pipeline_name = 'spiir'
	else:
		pipeline_name = 'gstlal'

	ntrig = len(eval(cat_name).keys())
	if ntrig == 0:
		return
	if item_name == 'snr':
		# inj vs. pipeline
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_%s_%s_%s.png" % (item_name, pipeline_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			if pipeline_name == 'gstlal':
				pipeline_item = [eval(cat_name).values()[idx]['gstlal'][ifo]['snr'] if ifo in eval(cat_name).values()[idx]['gstlal'].keys() else 0 for idx in range(0, ntrig) ]
			else:
				pipeline_item = [eval(cat_name).values()[idx]['spiir'][spiir_item_name] for idx in range(0, ntrig)]
			inj_item = [eval(cat_name).values()[idx]['inj'][spiir_item_name] for idx in range(0, ntrig)]
			max_x = int(max(inj_item)) +1
			max_y = int(max(pipeline_item)) +1
			axs[iifo].scatter(inj_item, pipeline_item)
			axs[iifo].plot(range(0,max_x), range(0,max_x))
			axs[iifo].set_xlim([0, max_x + 2])
			axs[iifo].set_ylim([0, max_x + 2])
			axs[iifo].set_xlabel("inj %s" % item_name)
			axs[iifo].set_ylabel("%s %s" % (pipeline_name, item_name))
			axs[iifo].set_title("%s" % (ifo))
		fig.suptitle("inj vs. %s %s in %s" % (pipeline_name, item_name, cat_name.replace('_', '')))
		plt.savefig(output)


def plot_coinc_det_timediff(item_name, cat_name, far_name):

	ntrig = len(eval(cat_name).keys())
	if ntrig == 0:
		return

	if item_name == 'timediff':
		# inj vs. spiir, diff of spiir merger and inj end time
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .7)
		pipeline_name = 'spiir'
		output = "abs_%s_inj_%s_%s_%s_hist.png" % (item_name, pipeline_name, cat_name, far_name)
	
		for (iifo, ifo) in enumerate(plot_ifos):
			valid_idx = []
			spiir_time1_name = '%s_time' % ifo[0].lower()
			spiir_ns1_name = '%s_ns' % ifo[0].lower()
			spiir_time1 = [(eval(cat_name).values()[idx]['spiir'][spiir_time1_name]*1e9+eval(cat_name).values()[idx]['spiir'][spiir_ns1_name]) for idx in range(0, ntrig) ]
			inj_time1 = [(eval(cat_name).values()[idx]['inj'][spiir_time1_name]*1e9+eval(cat_name).values()[idx]['inj'][spiir_ns1_name]) for idx in range(0, ntrig)]
			for idx in range(0, ntrig):
				if spiir_time1[idx] > 0:
					valid_idx.append(idx)

			spiir_abs_timediff = [(spiir_time1[idx] - inj_time1[idx])/1e9 for idx in valid_idx] # in seconds

			axs[iifo].hist(spiir_abs_timediff, bins = 40, weights = np.ones(len(spiir_abs_timediff))/len(spiir_abs_timediff), range = (-0.2, 0.2))
			axs[iifo].set_xlabel("%s of spiir and inj end time (seconds)" % (item_name))
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_xlim([-0.2, 0.2])
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(spiir_abs_timediff)))
		fig.suptitle("%s of %s and inj end time in %s" % (item_name, pipeline_name, cat_name.replace('_', '')))
		plt.savefig(output)

		# inj vs. gstlal
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .7)
		pipeline_name = 'gstlal'
		output = "abs_%s_inj_%s_%s_%s_hist.png" % (item_name, pipeline_name, cat_name, far_name)
	
		for (iifo, ifo) in enumerate(plot_ifos):
			valid_idx = []
			spiir_time1_name = '%s_time' % ifo[0].lower()
			spiir_ns1_name = '%s_ns' % ifo[0].lower()

			gstlal_time1 = [(eval(cat_name).values()[idx]['gstlal'][ifo]['time']*1e9+eval(cat_name).values()[idx]['gstlal'][ifo]['time_ns']) if ifo in eval(cat_name).values()[idx]['gstlal'] else 0 for idx in range(0, ntrig) ]
			inj_time1 = [(eval(cat_name).values()[idx]['inj'][spiir_time1_name]*1e9+eval(cat_name).values()[idx]['inj'][spiir_ns1_name]) for idx in range(0, ntrig)]

			for idx in range(0, ntrig):
				if gstlal_time1[idx] > 0:
					valid_idx.append(idx)

			gstlal_abs_timediff = [(gstlal_time1[idx] - inj_time1[idx])/1e9 for idx in valid_idx] # in nanoseconds

			axs[iifo].hist(gstlal_abs_timediff, bins = 40, weights = np.ones(len(gstlal_abs_timediff))/len(gstlal_abs_timediff), range = (-0.2, 0.2))
			axs[iifo].set_xlabel("%s of gstlal and inj end time (seconds)" % (item_name))
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_xlim([-0.2, 0.2])
			axs[iifo].set_title("%s (nevent %d)" % (ifo, len(gstlal_abs_timediff)))
		fig.suptitle("%s of gstlal and inj end time in %s" % (item_name, cat_name.replace('_', '')))
		plt.savefig(output)



		# inj vs. spiir, diff of spiir diff and inj diff
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .7)
		pipeline_name = 'spiir'
		output = "diff_%s_inj_%s_%s_%s_hist.png" % (item_name, pipeline_name, cat_name, far_name)
	
		for (iifo, ifo_pair) in enumerate(plot_ifo_pairs):
			valid_idx = []
			spiir_time1_name = '%s_time' % ifo_pair[0].lower()
			spiir_ns1_name = '%s_ns' % ifo_pair[0].lower()
			spiir_time2_name = '%s_time' % ifo_pair[2].lower()
			spiir_ns2_name = '%s_ns' % ifo_pair[2].lower()
			spiir_time1 = [(eval(cat_name).values()[idx]['spiir'][spiir_time1_name]*1e9+eval(cat_name).values()[idx]['spiir'][spiir_ns1_name]) for idx in range(0, ntrig) ]
			spiir_time2 = [(eval(cat_name).values()[idx]['spiir'][spiir_time2_name]*1e9+eval(cat_name).values()[idx]['spiir'][spiir_ns2_name]) for idx in range(0, ntrig) ]
			inj_time1 = [(eval(cat_name).values()[idx]['inj'][spiir_time1_name]*1e9+eval(cat_name).values()[idx]['inj'][spiir_ns1_name]) for idx in range(0, ntrig)]
			inj_time2 = [(eval(cat_name).values()[idx]['inj'][spiir_time2_name]*1e9+eval(cat_name).values()[idx]['inj'][spiir_ns2_name]) for idx in range(0, ntrig)]
			for idx in range(0, ntrig):
				if spiir_time1[idx] > 0 and spiir_time2[idx] >0:
					valid_idx.append(idx)

			spiir_diff_timediff = [(spiir_time1[idx] - spiir_time2[idx]) - (inj_time1[idx] - inj_time2[idx])for idx in valid_idx] # in nanoseconds

			axs[iifo].hist(spiir_diff_timediff, bins = 40, weights = np.ones(len(spiir_diff_timediff))/len(spiir_diff_timediff), range = (-2000000, 2000000))
			axs[iifo].set_xlabel("spiir %s - inj %s (nanoseconds)" % (item_name, item_name))
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_xlim([-2000000, 2000000])
			axs[iifo].set_title("%s (nevent %d)" % (ifo_pair, len(spiir_diff_timediff)))
		fig.suptitle("%s - inj %s in %s" % (pipeline_name, item_name, cat_name.replace('_', '')))
		plt.savefig(output)

		# inj vs. gstlal
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .7)
		pipeline_name = 'gstlal'
		output = "diff_%s_inj_%s_%s_%s_hist.png" % (item_name, pipeline_name, cat_name, far_name)
	
		for (iifo, ifo_pair) in enumerate(plot_ifo_pairs):
			valid_idx = []
			ifo1 = ifo_pair[0:2]
			ifo2 = ifo_pair[2:4]
			spiir_time1_name = '%s_time' % ifo1[0].lower()
			spiir_ns1_name = '%s_ns' % ifo1[0].lower()
			spiir_time2_name = '%s_time' % ifo2[0].lower()
			spiir_ns2_name = '%s_ns' % ifo2[0].lower()

			gstlal_time1 = [(eval(cat_name).values()[idx]['gstlal'][ifo1]['time']*1e9+eval(cat_name).values()[idx]['gstlal'][ifo1]['time_ns']) if ifo1 in eval(cat_name).values()[idx]['gstlal'] else 0 for idx in range(0, ntrig) ]
			gstlal_time2 = [(eval(cat_name).values()[idx]['gstlal'][ifo2]['time']*1e9+eval(cat_name).values()[idx]['gstlal'][ifo2]['time_ns']) if ifo2 in eval(cat_name).values()[idx]['gstlal'] else 0 for idx in range(0, ntrig) ]
			inj_time1 = [(eval(cat_name).values()[idx]['inj'][spiir_time1_name]*1e9+eval(cat_name).values()[idx]['inj'][spiir_ns1_name]) for idx in range(0, ntrig)]
			inj_time2 = [(eval(cat_name).values()[idx]['inj'][spiir_time2_name]*1e9+eval(cat_name).values()[idx]['inj'][spiir_ns2_name]) for idx in range(0, ntrig)]

			for idx in range(0, ntrig):
				if gstlal_time1[idx] > 0 and gstlal_time2[idx] >0:
					valid_idx.append(idx)

			gstlal_diff_timediff = [(gstlal_time1[idx] - gstlal_time2[idx]) - (inj_time1[idx] - inj_time2[idx]) for idx in valid_idx] # in nanoseconds

			axs[iifo].hist(gstlal_diff_timediff, bins = 40, weights = np.ones(len(gstlal_diff_timediff))/len(gstlal_diff_timediff), range = (-2000000, 2000000))
			axs[iifo].set_xlabel("spiir %s - inj %s (nanoseconds)" % (item_name, item_name))
			axs[iifo].set_ylabel("Histogram")
			axs[iifo].set_xlim([-2000000, 2000000])
			axs[iifo].set_title("%s (nevent %d)" % (ifo_pair, len(gstlal_diff_timediff)))
		fig.suptitle("%s - inj %s in %s" % (pipeline_name, item_name, cat_name.replace('_', '')))
		plt.savefig(output)




def plot_coinc_det_far(item_name, cat_name, far_name):
	# gstlal item vs. spiir item in far
	plt.figure()
	output = "%s_gstlal_spiir_%s_%s.png" % (item_name, cat_name, far_name)
	ntrig = len(eval(cat_name).keys())

	gstlal_item = [eval(cat_name).values()[idx]['gstlal'].values()[0][item_name] for idx in range(0, ntrig) ]
	spiir_item = [eval(cat_name).values()[idx]['spiir'][item_name] for idx in range(0, ntrig)]
	gstlal_item = np.log10(gstlal_item)
	min_x = int(min(gstlal_item)) -1
	try:
		spiir_item = np.log10(spiir_item)
	except:
		spiir_item = 0

	plt.scatter(gstlal_item, spiir_item)
	plt.plot(range(min_x, 0), range(min_x, 0))
	plt.xlim([min_x - 2, 0])
	plt.ylim([min_x - 2, 0])
	plt.ylabel("spiir log10(%s) Hz" % item_name)
	plt.xlabel("gstlal log10(%s) Hz" % item_name)
	plt.title("gstlal vs. spiir %s in %s" % (item_name, cat_name.replace('_', '')))
	plt.savefig(output)

	# spiir time vs. snr and far
	fig = plt.figure()
	axes = fig.gca()
	output = "%s_time_spiir_%s_%s.png" % (item_name, cat_name, far_name)
	ntrig = len(eval(cat_name).keys())

	time_item = eval(cat_name).keys()
	spiir_cohsnr = [eval(cat_name).values()[idx]['spiir']['cohsnr'] for idx in range(0, ntrig)]
	spiir_far = [eval(cat_name).values()[idx]['spiir']['far'] for idx in range(0, ntrig)]

	cb = plt.scatter(time_item, spiir_cohsnr, c=spiir_far, norm=matplotlib.colors.LogNorm(), vmin=1e-13, vmax=1e-3, linewidth=0.2, alpha=0.8)
	fig.colorbar(cb, ax=axes).set_label("FAR (Hz)")
	plt.ylim([7, 13])
	plt.xlabel("time (s)")
	plt.ylabel("spiir cohsnr")
	plt.title("spiir %s in %s" % (item_name, cat_name.replace('_', '')))
	plt.savefig(output)


	# gstlal time vs. snr and far
	fig = plt.figure()
	axes = fig.gca()
	output = "%s_time_gstlal_%s_%s.png" % (item_name, cat_name, far_name)
	ntrig = len(eval(cat_name).keys())

	time_item = eval(cat_name).keys()
	gstlal_netsnr = [np.linalg.norm([eval(cat_name)[key]['gstlal'][this_key]['snr'] for this_key, this_value in eval(cat_name)[key]['gstlal'].items()]) for key,value in eval(cat_name).items()]
	gstlal_far = [eval(cat_name).values()[idx]['gstlal'].values()[0]['far'] for idx in range(0, ntrig)]

	cb = plt.scatter(time_item, gstlal_netsnr, c=gstlal_far, norm=matplotlib.colors.LogNorm(), vmin=1e-13, vmax=1e-3, linewidth=0.2, alpha=0.8)
	fig.colorbar(cb, ax=axes).set_label("FAR (Hz)")
	plt.xlabel("time (s)")
	plt.ylabel("gstlal network snr")
	plt.ylim([7, 13])
	plt.title("gstlal %s in %s" % (item_name, cat_name.replace('_', '')))
	plt.savefig(output)



def plot_coinc_det(item_name, cat_name, far_name):

	if item_name == 'mchirp':
		# gstlal vs. spiir
		plt.figure()
		output = "%s_gstlal_spiir_%s_%s.png" % (item_name, cat_name, far_name)
		ntrig = len(eval(cat_name).keys())
		gstlal_item = [calc_mchirp(eval(cat_name).values()[idx]['gstlal'].values()[0]['mass1'], eval(cat_name).values()[idx]['gstlal'].values()[0]['mass2']) for idx in range(0, ntrig)]
		inj_item = [eval(cat_name).values()[idx]['inj']['mchirp'] for idx in range(0, ntrig)]
		spiir_item = [eval(cat_name).values()[idx]['spiir']['mchirp'] for idx in range(0, ntrig)]
		max_x = int(max(gstlal_item)) +1
		max_y = int(max(spiir_item)) +1
		plt.scatter(gstlal_item, spiir_item)
		plt.plot(range(0,max_x), range(0,max_x))
		plt.xlim([0, max_x ])
		plt.ylim([0, max_x ])
		plt.xlabel("gstlal %s" % item_name)
		plt.ylabel("spiir %s" % item_name)
		plt.title("gstlal vs. spiir %s in %s" % (item_name, cat_name.replace('_', '')))
		plt.savefig(output)
		plt.figure()

		# inj vs. spiir
		plt.figure()
		output = "%s_inj_spiir_%s_%s.png" % (item_name, cat_name, far_name)
		max_x = int(max(inj_item)) +1
		max_y = int(max(spiir_item)) +1
		plt.scatter(inj_item, spiir_item)
		plt.plot(range(0,max_x), range(0,max_x))
		plt.xlim([0, max_x ])
		plt.ylim([0, max_x ])
		plt.xlabel("inj %s" % item_name)
		plt.ylabel("spiir %s" % item_name)
		plt.title("inj vs. spiir %s in %s" % (item_name, cat_name.replace('_', '')))
		plt.savefig(output)

		# inj vs. spiir hist
		plt.figure()
		output = "%s_inj_spiir_%s_%s_hist.png" % (item_name, cat_name, far_name)
		ratio = [spiir_item[idx]/inj_item[idx] for idx in range(0, ntrig)]
		plt.hist(ratio, bins = 40, weights = np.ones(len(ratio))/len(ratio))
		plt.xlabel("spiir/inj %s" % item_name)
		plt.ylabel("Histogram")
		plt.title("nevent %d" % (len(ratio)))
		plt.savefig(output)
	

		# gstlal vs. spiir
		plt.figure()
		output = "%s_gstlal_spiir_%s_%s_hist.png" % (item_name, cat_name, far_name)
		ratio = [spiir_item[idx]/gstlal_item[idx] for idx in range(0, ntrig)]
		plt.hist(ratio, bins = 40, weights = np.ones(len(ratio))/len(ratio))
		plt.xlabel("spiir/gstlal %s" % item_name)
		plt.ylabel("Histogram")
		plt.title("nevent %d" % (len(ratio)))
		plt.savefig(output)
	
		return

	# snr or chisq plotting
	# gstlal item vs. spiir item
	plt.figure()
	fig,axs = plt.subplots(ndet, 1)
	fig.subplots_adjust(hspace = .5)
	output = "%s_gstlal_spiir_%s_%s.png" % (item_name, cat_name, far_name)
	ntrig = len(eval(cat_name).keys())
	
	for (iifo, ifo) in enumerate(plot_ifos):
		gstlal_item = [eval(cat_name).values()[idx]['gstlal'][ifo][item_name] if ifo in eval(cat_name).values()[idx]['gstlal'].keys() else 0 for idx in range(0, ntrig) ]
		spiir_item_name = '%s_%s' % (item_name, ifo[0].lower())
		spiir_item = [eval(cat_name).values()[idx]['spiir'][spiir_item_name] for idx in range(0, ntrig)]
		max_x = int(max(gstlal_item)) +1
		max_y = int(max(spiir_item)) +1
		axs[iifo].scatter(gstlal_item, spiir_item)
		axs[iifo].plot(range(0,max_x), range(0,max_x))
		axs[iifo].set_xlim([0, max_x + 2])
		axs[iifo].set_ylim([0, max_x + 2])
		axs[iifo].set_ylabel("spiir %s" % item_name)
		axs[iifo].set_xlabel("gstlal %s" % item_name)
		axs[iifo].set_title("%s" % (ifo))
	fig.suptitle("gstlal vs. spiir %s in %s" % (item_name, cat_name.replace('_', '')))

	plt.savefig(output)

	if item_name == 'snr':
		# inj vs. gstlal
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_gstlal_%s_%s.png" % (item_name, cat_name, far_name)
		
		for (iifo, ifo) in enumerate(plot_ifos):
			gstlal_item = [eval(cat_name).values()[idx]['gstlal'][ifo]['snr'] if ifo in eval(cat_name).values()[idx]['gstlal'].keys() else 0 for idx in range(0, ntrig) ]
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			inj_item = [eval(cat_name).values()[idx]['inj'][spiir_item_name] for idx in range(0, ntrig)]
			max_x = int(max(inj_item)) +1
			max_y = int(max(gstlal_item)) +1
			axs[iifo].scatter(inj_item, gstlal_item)
			axs[iifo].plot(range(0,max_x), range(0,max_x))
			axs[iifo].set_xlim([0, max_x + 2])
			axs[iifo].set_ylim([0, max_x + 2])
			axs[iifo].set_xlabel("inj %s" % item_name)
			axs[iifo].set_ylabel("gstlal %s" % item_name)
			axs[iifo].set_title("%s" % (ifo))
		fig.suptitle("inj vs. gstlal %s in %s" % (item_name, cat_name.replace('_', '')))
		plt.savefig(output)
	
		plt.figure()
		fig,axs = plt.subplots(ndet, 1)
		fig.subplots_adjust(hspace = .5)
		output = "%s_inj_spiir_%s_%s.png" % (item_name, cat_name, far_name)
	
		# inj snr vs. spiir snr
		for (iifo, ifo) in enumerate(plot_ifos):
			spiir_item_name = 'snr_%s' % ifo[0].lower()
			spiir_item = [eval(cat_name).values()[idx]['spiir'][spiir_item_name] for idx in range(0, ntrig)]
			inj_item = [eval(cat_name).values()[idx]['inj'][spiir_item_name] for idx in range(0, ntrig)]
			max_x = int(max(inj_item)) + 1
			max_y = int(max(spiir_item)) + 1
			axs[iifo].scatter(inj_item, spiir_item)
			axs[iifo].plot(range(0,max_x), range(0,max_x))
			axs[iifo].set_xlim([0, max_x + 2])
			axs[iifo].set_ylim([0, max_x + 2])
			axs[iifo].set_ylabel("spiir %s" % item_name)
			axs[iifo].set_xlabel("inj %s" % item_name)
			axs[iifo].set_title("%s" % (ifo))
		fig.suptitle("inj vs. spiir %s in %s" % (item_name, cat_name.replace('_', '')))
	
		plt.savefig(output)


def get_nevent_found(trig_dict, thresh, is_gstlal):
	nfound = 0
	if is_gstlal:
		for key, value in trig_dict.items():
			if value.values()[0]['far'] > 0.0 and value.values()[0]['far'] < thresh:
				nfound += 1
	else:
		for key, value in trig_dict.items():
			if spiir_crite(value, thresh):
				nfound += 1

	return nfound


def ifo_crite(this_event):
	if 'L1' in this_event.keys() and 'H1' in this_event.keys():
		return True
	else:
		return False

def time_crite(inj_time, event_dict):
	test = [inj_time+1 in event_dict, inj_time in event_dict, inj_time -1 in event_dict]
	if sum(test) >= 1:
		return True
	else:
		return False

def spiir_crite(this_spiir, thresh):
	single_thresh = 0.5
	ifo_fars_ok=[this_spiir['far_h'] < single_thresh and this_spiir['far_h'] > 0., this_spiir['far_l'] < single_thresh and this_spiir['far_l'] > 0., this_spiir['far_v'] < single_thresh and this_spiir['far_v'] > 0. ]
	
	if this_spiir['far'] < thresh and this_spiir['far'] >0. and sum(ifo_fars_ok) >= 2:
		return True
	else:
		return False

def mass_crite(mass1, mass2):
	if mass1 > 1.1 and mass2 > 1.1 and mass1 <3. and mass2 < 3. :
		return True
	else:
		return False

def mchirp_crite(this_mchirp, inj_mchirp):
	if abs(this_mchirp - inj_mchirp)/ inj_mchirp < 0.1 and this_mchirp > 0.965 and this_mchirp < 2.563:
		return True
	else:
		return False

def calc_eta(mass1, mass2):
		return mass1 * mass2 / (mass1 + mass2) **2.

def calc_mchirp(mass1, mass2):
		return (mass1 + mass2) * calc_eta(mass1, mass2)**0.6

print "---------------------------------"
print "far_thresh to select detections %.4g" % far_thresh
spiir_nfar_none = 0
spiir_nfar_zero = 0

# load injection all
inj_all = {}
inj_exc = {}
fsim = open(sim_name, 'r')
lines_sngl = fsim.readlines()
nevent_all = 0
nevent_inrange = 0
for line in lines_sngl:
	h_end_time, h_ns, l_time, l_ns, v_time, v_ns, mchirp, eff_dist_l, snr_h, snr_l, snr_v, mass1, mass2, spin1z, spin2z = line.split(',')
	nevent_all += 1
	this_inj = {'h_time': int(h_end_time), 'h_ns': int(h_ns), 'l_time': int(l_time), 'l_ns': int(l_ns), 'v_time': int(v_time), 'v_ns': int(v_ns), 'eff_dist_l':float(eff_dist_l), 'mchirp':float(mchirp), 'snr_h': float(snr_h), 'snr_l': float(snr_l), 'snr_v': float(snr_v), 'mass1': float(mass1), 'mass2': float(mass2), 'spin1z': float(spin1z), 'spin2z': float(spin2z)}
	if mass_crite_on_injection and not mass_crite(float(mass1), float(mass2)): # need to exclude
		inj_exc.setdefault(int(h_end_time), this_inj)
	else: 
		inj_all.setdefault(int(h_end_time), this_inj)
		nevent_inrange += 1
print "---------------------------------"
print " injections:"
print "number of injections %d" % nevent_all
print "number of injections both mass > 1.1 and < 3 solar mass %d" % nevent_inrange

all_f = os.listdir(spiir_result_dir)
spiir_all = {}
spiir_dets = ["%s/%s" % (spiir_result_dir,fname) for fname in all_f if 'sngl_far_118' in fname]
for fsngl_name in spiir_dets:
	fsngl = open(fsngl_name, 'r')
	lines_sngl = fsngl.readlines()
	for line in lines_sngl:
		sid, far, mchirp, eff_dist_l, ifos, cohsnr, snr_h, snr_l, snr_v, chisq_h, chisq_l, chisq_v, mchirp_det, h_end_time, far_h, far_l, far_v, mass1, mass2, rank, nullsnr, h_time, h_ns, l_time, l_ns, v_time, v_ns = line.split(',')
	
		try:	
			this_far = float(far)
			#print i, far, sid
			this_spiir = {'sid': sid, 'far': float(far), 'cohsnr': float(cohsnr), 'snr_h': float(snr_h), 'snr_l': float(snr_l), 'snr_v': float(snr_v), 'chisq_h': float(chisq_h), 'chisq_l': float(chisq_l), 'chisq_v':float(chisq_v), 'eff_dist_l': float(eff_dist_l), 'mchirp': float(mchirp_det), 'far_h': float(far_h), 'far_l': float(far_l), 'far_v': float(far_v) if far_v != '' else 0.0, 'ifos': ifos, 'mass1': float(mass1), 'mass2': float(mass2), 'rank':float(rank), 'nullsnr': float(nullsnr), 'h_time': int(h_time), 'h_ns': int(h_ns), 'l_time': int(l_time), 'l_ns': int(l_ns), 'v_time': int(v_time), 'v_ns': int(v_ns)}
	
		except:
			#print i, far, sid
			spiir_nfar_none += 1
			# set None Far to be 0.0
			this_spiir = {'sid': sid, 'far': 0.0, 'cohsnr': float(cohsnr), 'snr_h': float(snr_h), 'snr_l': float(snr_l), 'snr_v': float(snr_v), 'chisq_h': float(chisq_h), 'chisq_l': float(chisq_l), 'chisq_v':float(chisq_v), 'eff_dist_l': float(eff_dist_l), 'mchirp': float(mchirp_det), 'far_h': 0.0, 'far_l': 0.0, 'far_v':  0.0, 'ifos': ifos,  'mass1': float(mass1), 'mass2': float(mass2), 'rank':float(rank), 'nullsnr': float(nullsnr), 'h_time': int(h_time), 'h_ns': int(h_ns), 'l_time': int(l_time), 'l_ns': int(l_ns), 'v_time': int(v_time), 'v_ns': int(v_ns)}
	
		if not int(h_end_time) in inj_exc.keys():
			spiir_all.setdefault(int(h_end_time), this_spiir)
	fsngl.close()

nevent_spiir_found = get_nevent_found(spiir_all, far_thresh, False)
print "---------------------------------"
print " SPIIR triggers:"
print "number of spiir triggers %d" % len(spiir_all.keys())
print "number of spiir detections %d" % nevent_spiir_found
print "---------------------------------"

############################
# get the bank information to filter gstlal triggers
############################
bank_mchirp = {}
bank_mass1 = {}
bank_mass2 = {}
with open(bank_mass, 'r') as f:
	lines = f.readlines()
	for oneline in lines:
		if oneline[0] == '/':
			search_r = re.search(r'\d{4}', oneline)
			bankid_full = search_r.group()
			bankid = bankid_full.lstrip('0')
			if bankid is '':
				bankid = '0'
			bank_mchirp.setdefault(bankid, [])
			bank_mass1.setdefault(bankid, [])
			bank_mass2.setdefault(bankid, [])
		else:
			bank_mchirp[bankid].append(float(oneline.split(',')[0]))
			bank_mass1[bankid].append("{0:.6f}".format(float(oneline.split(',')[2])))
			bank_mass2[bankid].append("{0:.6f}".format(float(oneline.split(',')[3])))

# load gstlal detections
fsngl = open(fsngl_gstlal_name, 'r')
lines_sngl = fsngl.readlines()
gstlal_all_original = {}

for line in lines_sngl:
	sid, far, mchirp, eff_dist_l, ifo, snr, chisq, mass1, mass2, h_end_time, spin1z, spin2z, time, time_ns = line.split(',')
	gstlal_all_original.setdefault(int(h_end_time), {})
	gstlal_all_original[int(h_end_time)][ifo] = {'sid': sid, 'far': float(far), 'snr': float(snr), 'chisq': float(chisq), 'eff_dist_l': float(eff_dist_l), 'mass1': float(mass1), 'mass2': float(mass2), 'spin1z': float(spin1z), 'spin2z': float(spin2z), 'time': int(time), 'time_ns': int(time_ns)}



# remove those detections that have masses out of spiir range, keep the same detection range as spiir
# remove HV, LV events
nevent_gstlal_HL = 0
gstlal_all = {}
gstlal_all_masscrite = {}
for time in gstlal_all_original.keys():
	this_gstlal = gstlal_all_original[time]
	if ndet > 2:
		if not time in inj_exc.keys():
			gstlal_all.setdefault(time, gstlal_all_original[time])
			if mass_crite(this_gstlal.values()[0]['mass1'], this_gstlal.values()[0]['mass2']):
				gstlal_all_masscrite.setdefault(time, gstlal_all_original[time])
	else: # ndet == 2
		if not time in inj_exc.keys() and ifo_crite(this_gstlal):
			nevent_gstlal_HL += 1
			gstlal_all.setdefault(time, gstlal_all_original[time])
			if mass_crite(this_gstlal.values()[0]['mass1'], this_gstlal.values()[0]['mass2']):
				gstlal_all_masscrite.setdefault(time, gstlal_all_original[time])


nevent_gstlal_found = get_nevent_found(gstlal_all, far_thresh, True)
nevent_gstlal_found_masscrite = get_nevent_found(gstlal_all_masscrite, far_thresh, True)
print " gstlal triggers:"
print "number of total gstlal triggers %d" % len(gstlal_all_original.keys())
if ndet == 2:
	print " - number of gstlal HL and HLV triggers %d" % nevent_gstlal_HL
print " -- number of gstlal triggers correspond to filtered injections %d" % len(gstlal_all.keys())
print " --- number of gstlal detections %d" % nevent_gstlal_found
print "     (number of gstlal detections with template mass in spiir research range %d)" % nevent_gstlal_found_masscrite
print "---------------------------------"

if mass_crite_on_gstlaldet:
	gstlal_all = gstlal_all_masscrite

both_found = {}
both_missed = {}
coinc_only_gstlal = {}
coinc_only_spiir = {}
coinc_both_missed = {}
coinc_only_gstlal_outrange = {}
coinc_only_spiir_outrange = {}
sngl_only_gstlal_found = {}
sngl_only_gstlal_found_outrange = {}
sngl_only_gstlal_missed = {}
sngl_only_spiir_found = {}
sngl_only_spiir_found_outrange = {}
sngl_only_spiir_missed = {}
nosngl_both_missed = {}
all_spiir_found = {}
all_gstlal_found = {}

for inj_time in inj_all.keys():
	if inj_time in gstlal_all:
		gstlal_far = gstlal_all[inj_time].values()[0]['far']
		if gstlal_far < far_thresh:
			all_gstlal_found.setdefault(inj_time, {'inj': inj_all[inj_time], 'gstlal': gstlal_all[inj_time]})


# find injections that have both gstlal and spiir corresponses, NOTE: unlike BBH detections, do not constrain mchirp to be within 10%
for inj_time in inj_all.keys():
	if inj_time in spiir_all:
		spiir_mchirp = spiir_all[inj_time]['mchirp']
		inj_mchirp = inj_all[inj_time]['mchirp']
		if spiir_crite(spiir_all[inj_time], far_thresh):
			all_spiir_found.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time]})

	if inj_time in gstlal_all and inj_time in spiir_all:
		spiir_mchirp = spiir_all[inj_time]['mchirp']
		inj_mchirp = inj_all[inj_time]['mchirp']
		gstlal_mchirp = calc_mchirp(gstlal_all[inj_time].values()[0]['mass1'], gstlal_all[inj_time].values()[0]['mass2'])
		spiir_far = spiir_all[inj_time]['far']
		gstlal_far = gstlal_all[inj_time].values()[0]['far']
		if mchirp_crite(spiir_mchirp, inj_mchirp) and mchirp_crite(gstlal_mchirp, inj_mchirp) and gstlal_far < far_thresh and spiir_crite(spiir_all[inj_time], far_thresh):
			both_found.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time], 'gstlal': gstlal_all[inj_time]})
		# only keep HL and HLV events
		if mchirp_crite(gstlal_mchirp, inj_mchirp) and gstlal_far < far_thresh and not spiir_crite(spiir_all[inj_time], far_thresh):
			coinc_only_gstlal.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time], 'gstlal': gstlal_all[inj_time]})

		if not mchirp_crite(gstlal_mchirp, inj_mchirp) and gstlal_far < far_thresh and not spiir_crite(spiir_all[inj_time], far_thresh):
			coinc_only_gstlal_outrange.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time], 'gstlal': gstlal_all[inj_time]})

		if mchirp_crite(spiir_mchirp, inj_mchirp) and gstlal_far > far_thresh and spiir_crite(spiir_all[inj_time], far_thresh):
			coinc_only_spiir.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time], 'gstlal': gstlal_all[inj_time]})

		if not mchirp_crite(spiir_mchirp, inj_mchirp) and gstlal_far > far_thresh and spiir_crite(spiir_all[inj_time], far_thresh):
			coinc_only_spiir_outrange.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time], 'gstlal': gstlal_all[inj_time]})

		if gstlal_far > far_thresh and not spiir_crite(spiir_all[inj_time], far_thresh):
			coinc_both_missed.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time], 'gstlal': gstlal_all[inj_time]})
	
	if inj_time in gstlal_all and not inj_time in spiir_all:
		inj_mchirp = inj_all[inj_time]['mchirp']
		gstlal_mchirp = calc_mchirp(gstlal_all[inj_time].values()[0]['mass1'], gstlal_all[inj_time].values()[0]['mass2'])
		gstlal_far = gstlal_all[inj_time].values()[0]['far']

		if mchirp_crite(gstlal_mchirp, inj_mchirp) and gstlal_far < far_thresh:
			sngl_only_gstlal_found.setdefault(inj_time, {'inj': inj_all[inj_time], 'gstlal': gstlal_all[inj_time]})
		else:
			sngl_only_gstlal_missed.setdefault(inj_time, {'inj': inj_all[inj_time], 'gstlal': gstlal_all[inj_time]})

	
	if inj_time in spiir_all and not inj_time in gstlal_all:
		spiir_mchirp = spiir_all[inj_time]['mchirp']
		inj_mchirp = inj_all[inj_time]['mchirp']
		spiir_far = spiir_all[inj_time]['far']

		if mchirp_crite(spiir_mchirp, inj_mchirp) and spiir_crite(spiir_all[inj_time], far_thresh):
			sngl_only_spiir_found.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time]})
		elif not mchirp_crite(spiir_mchirp, inj_mchirp) and spiir_crite(spiir_all[inj_time], far_thresh):
			sngl_only_spiir_found_outrange.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time]})
		else:
			sngl_only_spiir_missed.setdefault(inj_time, {'inj': inj_all[inj_time], 'spiir': spiir_all[inj_time]})
	
	if not inj_time in spiir_all and not inj_time in gstlal_all:
		nosngl_both_missed.setdefault(inj_time, {'inj': inj_all[inj_time]})

# those gstlal detections not in both_found nor sngl_only_gstlal_found nor coinc_only_gstlal
wth = {}
for time in gstlal_all.keys():
	if gstlal_all[time].values()[0]['far']< far_thresh:
		if not (time in both_found or time in sngl_only_gstlal_found or time in coinc_only_gstlal):
			wth.setdefault(time, gstlal_all[time])

# find out the reason of spiir lowchisq
spiir_lowchisq = {}
has_template = {}
nevent_day1 = 0
chisq_eps = 0.2
pdb.set_trace()
for time in sorted(all_spiir_found.keys()):
	m = all_spiir_found[time]
	mchirp = m['inj']['mchirp']
	if m['spiir']['chisq_h'] > chisq_eps and m['spiir']['chisq_l'] > chisq_eps:
		continue
	for bankid in bank_mchirp.keys():
		if mchirp > min(bank_mchirp[bankid]) and mchirp < max(bank_mchirp[bankid]):
			try:
				idx_mass1 = bank_mass1[bankid].index("{0:.6f}".format(m['gstlal'].values()[0]['mass1']))
				idx_mass2 = bank_mass2[bankid].index("{0:.6f}".format(m['gstlal'].values()[0]['mass2']))
				if idx_mass1 != idx_mass2:
					print "mismatch of idx of mass1 and 2", bankid, "{0:.6f}".format(m['gstlal'].values()[0]['mass1']),"{0:.6f}".format(m['gstlal'].values()[0]['mass2']), idx_mass1, idx_mass2
				has_template.setdefault(time, [bankid, 1, idx_mass1])
				spiir_lowchisq.setdefault(time, m)
			except:
				has_template.setdefault(time, [bankid, 0])
				spiir_lowchisq.setdefault(time, m)
			break

coinc_missed_close_inj = {}
coinc_missed_highsnr_inj = {}
for time in sorted(spiir_lowchisq.keys()):
	# eff_dist_l < 100 MPC:
	if spiir_lowchisq[time]['inj']['eff_dist_l'] < 10000:
		coinc_missed_close_inj.setdefault(time, spiir_lowchisq[time])
	inj_snr_l = spiir_lowchisq[time]['inj']['snr_l']
	inj_snr_h = spiir_lowchisq[time]['inj']['snr_h']
	spiir_snr_l = spiir_lowchisq[time]['spiir']['snr_l']
	this_spiir = spiir_lowchisq[time]['spiir']
	if spiir_snr_l < inj_snr_l * 0.9:
		coinc_missed_highsnr_inj.setdefault(time, spiir_lowchisq[time])

#pdb.set_trace()
with open(missed_fn, 'w') as f:
	f.write("time, bank id, tmplt id, inj snr_h, inj snr_l, spiir snr_h, spiir snr_l, inj mchirp, gstlal mchirp, spiir mchirp, inj mass1, inj mass2, gstlal mass1, gstlal mass2, spiir mass1, spiir mass2, spiir far, spiir far_h, spiir far_l\n")
	for time in sorted(coinc_missed_highsnr_inj.keys()):
		m = coinc_missed_highsnr_inj[time]
		f.write("%d,%s,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%.4g,%.4g,%.4g\n" % (time, has_template[time][0], has_template[time][2], m['inj']['snr_h'], m['inj']['snr_l'], m['spiir']['snr_h'], m['spiir']['snr_l'], m['inj']['mchirp'], calc_mchirp(m['gstlal'].values()[0]['mass1'], m['gstlal'].values()[0]['mass2']), m['spiir']['mchirp'], m['inj']['mass1'], m['inj']['mass2'], m['gstlal'].values()[0]['mass1'], m['gstlal'].values()[0]['mass2'], m['spiir']['mass1'], m['spiir']['mass2'], m['spiir']['far'], m['spiir']['far_h'], m['spiir']['far_l']))


# find out the reason of spiir misses and gstlal founds
nevent_veto_spiir_crite = 0
coinc_only_gstlal_inspiirbank = {}
coinc_only_gstlal_outspiirbank = {}
has_template = {}
nevent_day1 = 0
for time in sorted(coinc_only_gstlal.keys()):
	m = coinc_only_gstlal[time]
	mchirp = m['inj']['mchirp']
	if m['spiir']['far'] < far_thresh:
		nevent_veto_spiir_crite += 1
		continue
	if time < 1186729120:
		nevent_day1 += 1 
		#continue
	for bankid in bank_mchirp.keys():
		if mchirp > min(bank_mchirp[bankid]) and mchirp < max(bank_mchirp[bankid]):
			try:
				idx_mass1 = bank_mass1[bankid].index("{0:.6f}".format(m['gstlal'].values()[0]['mass1']))
				idx_mass2 = bank_mass2[bankid].index("{0:.6f}".format(m['gstlal'].values()[0]['mass2']))
				if idx_mass1 != idx_mass2:
					print "mismatch of idx of mass1 and 2", bankid, "{0:.6f}".format(m['gstlal'].values()[0]['mass1']),"{0:.6f}".format(m['gstlal'].values()[0]['mass2']), idx_mass1, idx_mass2
				has_template.setdefault(time, [bankid, 1, idx_mass1])
				coinc_only_gstlal_inspiirbank.setdefault(time, m)
			except:
				has_template.setdefault(time, [bankid, 0])
				coinc_only_gstlal_outspiirbank.setdefault(time, m)
			break

coinc_missed_close_inj = {}
coinc_missed_highsnr_inj = {}
for time in sorted(coinc_only_gstlal_inspiirbank.keys()):
	# eff_dist_l < 100 MPC:
	if coinc_only_gstlal[time]['inj']['eff_dist_l'] < 100:
		coinc_missed_close_inj.setdefault(time, coinc_only_gstlal[time])
	inj_snr_l = coinc_only_gstlal[time]['inj']['snr_l']
	inj_snr_h = coinc_only_gstlal[time]['inj']['snr_h']
	spiir_snr_l = coinc_only_gstlal[time]['spiir']['snr_l']
	this_spiir = coinc_only_gstlal[time]['spiir']
	if spiir_snr_l < inj_snr_l * 0.9:
		coinc_missed_highsnr_inj.setdefault(time, coinc_only_gstlal[time])

#pdb.set_trace()
with open(missed_fn, 'w') as f:
	f.write("time, bank id, tmplt id, inj snr_h, inj snr_l, spiir snr_h, spiir snr_l, inj mchirp, gstlal mchirp, spiir mchirp, inj mass1, inj mass2, gstlal mass1, gstlal mass2, spiir mass1, spiir mass2, spiir far, spiir far_h, spiir far_l\n")
	for time in sorted(coinc_missed_highsnr_inj.keys()):
		m = coinc_missed_highsnr_inj[time]
		f.write("%d,%s,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%.4g,%.4g,%.4g\n" % (time, has_template[time][0], has_template[time][2], m['inj']['snr_h'], m['inj']['snr_l'], m['spiir']['snr_h'], m['spiir']['snr_l'], m['inj']['mchirp'], calc_mchirp(m['gstlal'].values()[0]['mass1'], m['gstlal'].values()[0]['mass2']), m['spiir']['mchirp'], m['inj']['mass1'], m['inj']['mass2'], m['gstlal'].values()[0]['mass1'], m['gstlal'].values()[0]['mass2'], m['spiir']['mass1'], m['spiir']['mass2'], m['spiir']['far'], m['spiir']['far_h'], m['spiir']['far_l']))

nevent_both_found = len(both_found.keys())
nevent_gstlal_found_outinj = nevent_gstlal_found - nevent_both_found - len(coinc_only_gstlal.keys()) - len(sngl_only_gstlal_found.keys())
nevent_spiir_found_outinj = nevent_spiir_found - nevent_both_found - len(coinc_only_spiir.keys()) - len(sngl_only_spiir_found.keys())
print "inj <---> gstlal <---> SPIIR:"
print "number of both found %d" % len(both_found.keys())
print "number of only gstlal found in coincidence with spiir %d" % len(coinc_only_gstlal.keys())
print " - number of only gstlal found in coincidence with spiir and spiir veto due to single FAR test %d" % nevent_veto_spiir_crite
print " - number of only gstlal found whose template is out of spiir search range %d" % len(coinc_only_gstlal_outspiirbank.keys())
print " - number of only gstlal found whose template is in spiir search range %d" % len(coinc_only_gstlal_inspiirbank.keys())
print "number of only spiir found in coincidence with gstlal %d" % len(coinc_only_spiir.keys())
print "number of both missed in coincidence of gstlal and spiir %d" % len(coinc_both_missed.keys())
print "---------------------------------"
print "inj <---> gstlal:"
print "number of only gstlal found and within chirpmass range %d" % len(sngl_only_gstlal_found.keys())
print "number of only gstlal found and out of chirpmass range %d" % len(sngl_only_gstlal_found_outrange.keys())
print "number of gstlal missed and no spiir trigger found %d" % len(sngl_only_gstlal_missed.keys())
print "number of gstlal found in injections any mass < 1.1 %d" % nevent_gstlal_found_outinj
print "---------------------------------"
print "inj <---> SPIIR:"
print "number of only spiir found and within chirpmass range %d" % len(sngl_only_spiir_found.keys())
print "number of only spiir found and out of chirpmass range %d" % len(sngl_only_spiir_found_outrange.keys())
print "number of spiir missed and no gstlal trigger found %d" % len(sngl_only_spiir_missed.keys())
print "number of spiir found in injections any mass < 1.1 %d" % nevent_spiir_found_outinj
print "---------------------------------"
print "inj <---> None:"
print "number of injections not in gstlal nor spiir %d" % len(nosngl_both_missed.keys())



if far_thresh < 1e-5:
	far_name = "far1perm"
else:
	far_name = "farm5"

for cat_name in ["both_found", "coinc_only_gstlal_outspiirbank", "coinc_only_gstlal_inspiirbank"]:
	for item_name in ['snr', 'chisq', 'mchirp']:
		plot_coinc_det(item_name, cat_name, far_name)
	for item_name in ['far']:
		plot_coinc_det_far(item_name, cat_name, far_name)
	for item_name in ['timediff']:
		plot_coinc_det_timediff(item_name, cat_name, far_name)


for cat_name in ["sngl_only_gstlal_found", "all_spiir_found"]:
	for item_name in ['snr']:
		plot_sngl_det(item_name, cat_name, far_name)

#for cat_name in ["coinc_only_gstlal_inspiirbank"]:
for cat_name in ["both_found"]:
	for item_name in ['snr']:
		plot_inbank_det(item_name, cat_name, far_name)

plot_missfound(inj_all, gstlal_all, spiir_all, far_name)
